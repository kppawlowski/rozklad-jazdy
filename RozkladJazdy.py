#!/usr/bin/python3
# -*- coding: utf-8 -*-
import re
import csv
import os
import importlib
from mod_python import apache
from urlparse import parse_qsl

class Sciezka:
    @staticmethod
    def sprawdz_czas_przejazdu_sciezki(godziny_odj, godziny_prz):
        poczatek = godziny_odj[0]
        koniec = godziny_prz[-1]
        czas2s = koniec.split(":")
        czas1s = poczatek.split(":")
        result2 = (int(czas2s[0])*60) + (int(czas2s[1])*1) + (int(czas2s[2])*1/60)
        result1 = (int(czas1s[0])*60) + (int(czas1s[1])*1) + (int(czas1s[2])*1/60)
        return result2 - result1
    def __init__(self, godziny, godziny_prz, sciezka):
        self.godziny, self.godziny_prz, self.sciezka = godziny, godziny_prz, sciezka
        self.czas = self.sprawdz_czas_przejazdu_sciezki(godziny, godziny_prz)
    def godzinaPoczatku(self):
        return self.godziny[0]
    def godzinaKonca(self):
        return self.godziny_prz[-1]
    def __lt__(self, rhs):
        return self.godziny < rhs.godziny
    def __gt__(self, rhs):
        return self.godziny > rhs.godziny
    def __eq__(self, rhs):
        return self.godziny == rhs.godziny

class Przystanki:
    def __init__(self):
        self.przystanki = []
        self.idki = dict()
    def pobierz_id(self, nazwa):
        return self.idki.get(nazwa)
    def dodaj(self, nazwa):
        if self.idki.get(nazwa) is None:
            self.przystanki.append(nazwa)
            self.idki[nazwa] = len(self.przystanki)-1
            return len(self.przystanki)-1
        return self.pobierz_id(nazwa)
    def pobierz_nazwe(self, i):
        return self.przystanki[i]

class Rozklad():
    def __init__(self):
        self.database = []
        self.odjazdy = dict()
        self.przystanki = Przystanki()
        self.polaczenia = dict()
        self.wyszukana_sciezka = list()
        self.sprawdzona_sciezka = list()
        self.new_database = []
    def pobierz(self):
        self.new_database = dict()
        with open("/var/www/kacperpawlowski.pl/rozklady/Rozklad.csv") as csvfile:
            self.database = list(csv.reader(csvfile, delimiter=',', quotechar='|'))[1:]	
        for item in self.database:
            self.przystanki.dodaj(item[0])
            self.przystanki.dodaj(item[1])
        for item in self.database:
            item[0] = self.przystanki.pobierz_id(item[0])
            item[1] = self.przystanki.pobierz_id(item[1])
        for item in self.database:
            if self.new_database.get((item[0], item[1])) is None:
                self.new_database[(item[0], item[1])] = []
            self.new_database[(item[0], item[1])].append((item[4], item[2], item[3]))
            if self.polaczenia.get(item[0]) is None:
                self.polaczenia[item[0]] = set()
            self.polaczenia[item[0]].add(item[1])
        return None
    def generuj_przyjazdy(self):
        for item in self.database:
            if self.odjazdy.get(item[0]) is None:
                self.odjazdy[item[0]] = []
            self.odjazdy[item[0]].append(item[2])
    def znajdz_sciezke(self, aktualny, szukany, poprzednie=[]):
        if not poprzednie:
             self.wyszukana_sciezka = []
        if aktualny == szukany:
             self.wyszukana_sciezka += [poprzednie + [szukany]]
        elif aktualny not in poprzednie:
            for item in self.polaczenia.get(aktualny):
                self.znajdz_sciezke(item, szukany, poprzednie + [aktualny])
        else:
            return False
        return True
    def sprawdz_sciezke(self, oryginalna_sciezka, sciezka=None, przyjazd=None, godziny=[], godziny_prz=[]):
        if sciezka is None:
            sciezka = oryginalna_sciezka
        if sciezka and len(sciezka) >= 2:
            aktualna = self.new_database.get((sciezka[0], sciezka[1]))
            if aktualna:
                for item in aktualna:
                    if not godziny or (godziny and item[2] >= max(godziny) and item[1] >= max(godziny)):
                        self.sprawdz_sciezke(oryginalna_sciezka, sciezka[1:], item[2], godziny + [item[1]], godziny_prz + [przyjazd])
        else:
            self.sprawdzona_sciezka.append(Sciezka(godziny, godziny_prz + [przyjazd], oryginalna_sciezka))

    def wyszukaj_polaczenia(self, od, do):
        self.sprawdzona_sciezka=[]
        self.znajdz_sciezke(od, do)
        for item in self.wyszukana_sciezka:
            self.sprawdz_sciezke(item)
        self.sprawdzona_sciezka.sort()
        nowa_sciezka = []
        nowa_sciezka_ = []
        aktualny = None
        for item in self.sprawdzona_sciezka:
            if not aktualny or aktualny != item.godzinaPoczatku():
                aktualny = item.godzinaPoczatku()
                nowa_sciezka += [item]
        min_czasy = dict()
        for item in nowa_sciezka:
            if not min_czasy.get(item.godzinaKonca()) or min_czasy.get(item.godzinaKonca()) > item.czas:
                min_czasy[item.godzinaKonca()] = item.czas
        for item in nowa_sciezka:
            if item.czas == min_czasy.get(item.godzinaKonca()):
                nowa_sciezka_.append(item)
        return nowa_sciezka_

def generuj_przystanki_option(req, rozklad):
    for i, item in enumerate(rozklad.przystanki.przystanki):
        req.write("<option value={0}>{1}</option>".format(str(i), item))

def generuj_przystanki_select(req, rozklad, nazwa_pola):
    req.write("<select name=\"{0}\">".format(nazwa_pola))
    generuj_przystanki_option(req, rozklad)
    req.write('</select>&nbsp;')

def generuj_szukajke(req, rozklad):
    req.write('<form method="GET">')
    generuj_przystanki_select(req, rozklad, "o")
    generuj_przystanki_select(req, rozklad, "p")
    req.write('<input type=\"submit\" value=\"Szukaj\" name=\"search\">')
    req.write('</form>')

def handler(req):
    req.content_type = "text/html"
    rozklad = Rozklad()
    rozklad.pobierz()
    rozklad.generuj_przyjazdy()
    req.write("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title>Testowa nieoficjalna wyszukiwarka polaczen komunikacji miejskiej w Złotoryi</title><style>td { border: 1px solid black }</style></head><body>")
    generuj_szukajke(req, rozklad)
    if req.args:
        pairs = dict(parse_qsl(req.args))
        o = int(pairs.get("o"))
        p = int(pairs.get("p"))
        if o == p:
            req.write("Podaj inne miejsce docelowe niż miejsce startu</body></html>")
            return apache.OK
        req.write("Z: {0}<br />Do: {1}".format(rozklad.przystanki.pobierz_nazwe(o), rozklad.przystanki.pobierz_nazwe(p)))
        wyszukane = rozklad.wyszukaj_polaczenia(o,p)
        req.write("<br />Znaleziono {0} połączeń: <br/><br /> ".format(len(wyszukane)))
        req.write("<table style=\"border:1px black\"><tr><td>Przystanek</td><td>Przyjazd</td><td>Odjazd</td></tr>")
        for _id, item in enumerate(wyszukane):
            req.write("<tr><td colspan=3><center>Czas przejazdu: {1} minut</center></td</tr>".format(_id+1, str(item.czas)))
            for i, przystanki in enumerate(item.sciezka):
                if i == len(item.sciezka)-1:
                     req.write("<tr><td>{0}</td><td>{1}</td><td></td></tr>".format(rozklad.przystanki.pobierz_nazwe(przystanki), str(item.godziny_prz[i])))
                elif i == 0:
                     req.write("<tr><td>{0}</td><td></td><td>{1}</td></tr>".format(rozklad.przystanki.pobierz_nazwe(przystanki), str(item.godziny[i])))
                else:
                     req.write("<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>".format(rozklad.przystanki.pobierz_nazwe(przystanki), str(item.godziny_prz[i]), str(item.godziny[i])))
        req.write("</table><br />")
    req.write("<br /><strong>Wyszukiwarka jest w fazie prototypowej, może zawierać błędy</strong><br />&copy; <a href=\"http://kacperpawlowski.pl\">Kacper Pawłowski</a> 2019</body></html>")
    return apache.OK
